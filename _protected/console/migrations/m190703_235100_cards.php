<?php

use yii\db\Schema;
use yii\db\Migration;

class m190703_235100_cards extends Migration
{
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('cards', [
            'id' => Schema::TYPE_PK,
            'name' => Schema::TYPE_TEXT . ' COMMENT \'Наименование\'',
            'description' => Schema::TYPE_TEXT . ' COMMENT \'Описание\'',
            'image' => Schema::TYPE_TEXT . ' COMMENT \'Изображение\'',
            'view_count' => Schema::TYPE_INTEGER . ' DEFAULT 0 COMMENT \'Просмотров\'',
            'created_by' => Schema::TYPE_INTEGER . ' COMMENT \'Создано кем\'',
            'updated_by' => Schema::TYPE_INTEGER . ' COMMENT \'Обновлено кем\'',
            'created_at' => Schema::TYPE_INTEGER . ' NOT NULL COMMENT \'Обновлено\'',
            'updated_at' => Schema::TYPE_INTEGER . ' NOT NULL COMMENT \'Создано\'',
            'deleted' => Schema::TYPE_BOOLEAN . ' COMMENT \'Удалено\'',
            'deleted_at' => Schema::TYPE_INTEGER . ' COMMENT \'Удалено\'',
            'deleted_by' => Schema::TYPE_INTEGER . ' COMMENT \'Удалено кем\'',
            'lock' => Schema::TYPE_BOOLEAN . ' DEFAULT 0',
        ], $tableOptions);
        $this->addForeignKey('fk_cards_user_created', 'cards', 'created_by', '{{%user}}', 'id', 'CASCADE', 'SET NULL');
        $this->addForeignKey('fk_cards_user_updated', 'cards', 'updated_by', '{{%user}}', 'id', 'CASCADE', 'SET NULL');
        $this->addForeignKey('fk_cards_user_deleted', 'cards', 'deleted_by', '{{%user}}', 'id', 'CASCADE', 'SET NULL');
    }

    public function safeDown()
    {
        $this->dropTable('cards');
    }
}
