<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\Cards */

$this->title = 'Копирование карточки: '. ' ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Карточки', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Копирование';
?>
<div class="cards-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
    'model' => $model,
    ]) ?>

</div>
