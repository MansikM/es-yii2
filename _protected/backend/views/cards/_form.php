<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model backend\models\Cards */
/* @var $form yii\widgets\ActiveForm */

?>

<div class="cards-form">

    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>

    <?= $form->errorSummary($model); ?>

    <?= $form->field($model, 'id', ['template' => '{input}'])->textInput(['style' => 'display:none']); ?>

    <?= $form->field($model, 'name')->textInput(['placeholder' => 'Наименование']) ?>

    <?= $form->field($model, 'description')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'view_count')->textInput(['placeholder' => 'Количество просмотров']) ?>

    <?= $form->field($model, 'lock', ['template' => '{input}'])->textInput(['style' => 'display:none']); ?>

    <?= Html::label('Изображение', 'image') ?>
    <?= Html::tag('br') ?>
    <?= Html::img($model->getUploadUrl(), ['class' => 'rounded float-left img-thumbnail', 'style' => 'margin-bottom: 15px', 'id' => 'image']) ?>

    <div class="UploadDiv">
        <?= $form->field($model, 'image')->label('Загрузить изображение' ,['class'=>'btn btn-success', 'style' => 'color: white;'])->fileInput(['style' => 'display: none']) ?>
        <?= Html::a('После выбора файла, нажмите Сохранить!', null, ['class' => 'upload_response bg-success']) ?>
    </div>

    <?php $this->registerCssFile(Url::base('http').'/../assets/my.css'); ?>

    <div class="form-group">
    <?php if(Yii::$app->controller->action->id != 'save-as-new'): ?>
        <?= Html::submitButton($model->isNewRecord ? 'Сохранить' : 'Обновить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    <?php endif; ?>
    <?php if(Yii::$app->controller->action->id != 'create'): ?>
        <?= Html::submitButton('Сохранить копию', ['class' => 'btn btn-info', 'value' => '1', 'name' => '_asnew']) ?>
    <?php endif; ?>
        <?= Html::a(Yii::t('app', 'Отмена'), Yii::$app->request->referrer , ['class'=> 'btn btn-danger']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
