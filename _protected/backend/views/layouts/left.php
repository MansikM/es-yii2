<?php

use common\models\User;

/* @var $directoryAsset string */
?>

<aside class="main-sidebar">

    <section class="sidebar">

        <?php
        $menuItems = [];

        $menuItems[] = ['label' => 'Меню', 'options' => ['class' => 'header']];

        if (Yii::$app->user->can(User::ROLE_ADMIN)) {
            $menuItems[] = ['label' => 'Карточки', 'icon' => 'home', 'url' => ['/cards/index']];
        }

        if (Yii::$app->user->can(User::ROLE_MEMBER)) {
            $menuItems[] = ['label' => 'Главная', 'icon' => 'home', 'url' => ['/']];

            // TODO add logs
            $menuItems[] = ['label' => 'Лог сервера', 'icon' => 'server', 'url' => ['/logs/log/index']];

            if (Yii::$app->user->can(User::ROLE_ADMIN)) {
                // TODO add archive
                $menuItems[] = ['label' => 'Лог архив', 'icon' => 'archive', 'url' => '#', 'items' => [
                    ['label' => 'Лог архив сервера', 'icon' => 'server', 'url' => ['/logs/log-archive/index']],
                ]];
                $menuItems[] = ['label' => 'Пользователи', 'icon' => 'users', 'url' => ['/user/index']];
            }

            //$menuItems[] = ['label' => 'Api docs runner', 'icon' => 'book', 'url' => ['/documentation']];

            /*$menuItems[] = [
                'label' => 'Docs',
                'url' => ['/doc/page', 'view' => 'guide-README.html'],
                'template' => '<a href="{url}" target="_blank">
                        <i class="fa fa-external-link"></i>
                        {label}
                    </a>'
            ];*/
        }

        ?>

        <?= dmstr\widgets\Menu::widget(
            [
                'options' => ['class' => 'sidebar-menu tree', 'data-widget'=> 'tree'],
                'items' => $menuItems,
            ]
        ) ?>

    </section>

</aside>
