<?php

namespace backend\models\base;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;
use yii\helpers\Url;
use yiidreamteam\upload\ImageUploadBehavior;
/**
 * This is the base model class for table "cards".
 *
 * @property integer $id
 * @property string $name
 * @property string $description
 * @property string $image
 * @property integer $view_count
 * @property integer $created_by
 * @property integer $updated_by
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $deleted
 * @property integer $deleted_at
 * @property integer $deleted_by
 * @property integer $lock
 *
 * @property \common\models\User $createdBy
 * @property \common\models\User $deletedBy
 * @property \common\models\User $updatedBy
 */
class Cards extends \yii\db\ActiveRecord
{
    use \mootensai\relation\RelationTrait;

    private $_rt_softdelete;
    private $_rt_softrestore;

    public function __construct(){
        parent::__construct();
        $this->_rt_softdelete = [
            'deleted_by' => \Yii::$app->user->id,
            'deleted_at' => time(),
            'deleted' => 1,
        ];
        $this->_rt_softrestore = [
            'deleted_by' => null,
            'deleted_at' => null,
            'deleted' => null,
        ];
    }

    /**
    * This function helps \mootensai\relation\RelationTrait runs faster
    * @return array relation names of this model
    */
    public function relationNames()
    {
        return [
            'createdBy',
            'deletedBy',
            'updatedBy'
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'description'], 'string'],
            ['image', 'image', 'extensions' => 'jpg, jpeg, gif, png', 'on' => ['insert', 'update']],
            [['view_count', 'created_by', 'updated_by', 'created_at', 'updated_at', 'deleted_at', 'deleted_by'], 'integer'],
            [['created_at', 'updated_at'], 'required'],
            [['deleted', 'lock'], 'string', 'max' => 1],
            [['image', 'created_by', 'updated_by', 'deleted', 'deleted_at', 'deleted_by'], 'default', 'value' => null],
            [['view_count', 'lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'cards';
    }

    /**
     *
     * @return string
     * overwrite function optimisticLock
     * return string name of field are used to stored optimistic lock
     *
     */
    public function optimisticLock() {
        return 'lock';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Наименование',
            'description' => 'Описание',
            'image' => 'Изображение',
            'view_count' => 'Просмотров',
            'lock' => 'Lock',
        ];
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedBy()
    {
        return $this->hasOne(\common\models\User::class, ['id' => 'created_by']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDeletedBy()
    {
        return $this->hasOne(\common\models\User::class, ['id' => 'deleted_by']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUpdatedBy()
    {
        return $this->hasOne(\common\models\User::class, ['id' => 'updated_by']);
    }
    
    /**
     * @inheritdoc
     * @return array mixed
     */
    public function behaviors()
    {
        $rand = random_int(1000,9999);
        return [
            'timestamp' => [
                'class' => TimestampBehavior::class,
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new \yii\db\Expression('UNIX_TIMESTAMP(NOW())'),
            ],
            'blameable' => [
                'class' => BlameableBehavior::class,
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
            ],
            [
                'class' => ImageUploadBehavior::class,
                'attribute' => 'image',
                'thumbs' => [
                    'thumb' => ['width' => 50, 'height' => 50],
                ],
                'filePath' => '../uploads/[[filename]].[[extension]]',
                'fileUrl' => Url::base('http').'/../uploads/[[filename]].[[extension]]',
                'thumbPath' => '../uploads/[[filename]]_thumb.[[extension]]',
                'thumbUrl' => Url::base('http').'/../uploads/[[filename]]_thumb.[[extension]]',
                'maxImageWidth' => 256,
                'maxImageHeight' => 256,
                'newFileName' => '[[pk]]_'.$rand.'.[[extension]]'
            ],
        ];
    }

    /**
     * @param bool $thumb
     * @return mixed
     */
    public function getUploadUrl($thumb = false, $add_dots = true){
        $fname = $this->image;
        if ($thumb && !empty($fname)){
            $fname_tmp = explode('.', $fname);
            $ext = $fname_tmp[\count($fname_tmp)-1];
            $fname = str_replace('.'.$ext, '_thumb.'.$ext,  $fname);
        }

        $dots = $add_dots ? '/..' : '';

        $file_dir = \Yii::getAlias('@webroot').$dots.'/uploads/'.$fname;
        $file_url = Url::base('http') . $dots.'/uploads/'.$fname;
        $empty_dir = Url::base('http') .$dots. ($thumb ? "/uploads/empty_thumb.jpeg" : "/uploads/empty.jpeg");

        return is_file($file_dir) && file_exists($file_dir) ? $file_url : $empty_dir;
    }

    /**
     * The following code shows how to apply a default condition for all queries:
     *
     * ```php
     * class Customer extends ActiveRecord
     * {
     *     public static function find()
     *     {
     *         return parent::find()->where(['deleted' => false]);
     *     }
     * }
     *
     * // Use andWhere()/orWhere() to apply the default condition
     * // SELECT FROM customer WHERE `deleted`=:deleted AND age>30
     * $customers = Customer::find()->andWhere('age>30')->all();
     *
     * // Use where() to ignore the default condition
     * // SELECT FROM customer WHERE age>30
     * $customers = Customer::find()->where('age>30')->all();
     * ```
     */

    /**
     * @inheritdoc
     * @return \backend\models\CardsQuery the active query used by this AR class.
     */
    public static function find()
    {
        $query = new \backend\models\CardsQuery(get_called_class());
        return $query->where(['cards.deleted_by' => null]);
    }
}
