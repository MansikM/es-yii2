<?php

namespace backend\models;

use Yii;
use \backend\models\base\Cards as BaseCards;

/**
 * This is the model class for table "cards".
 */
class Cards extends BaseCards
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['name', 'description'], 'string'],
            ['image', 'image', 'extensions' => 'jpg, jpeg, gif, png', 'on' => ['insert', 'update']],
            [['view_count', 'created_by', 'updated_by', 'created_at', 'updated_at', 'deleted_at', 'deleted_by'], 'integer'],
            [['created_at', 'updated_at'], 'required'],
            [['deleted', 'lock'], 'string', 'max' => 1],
            [['image', 'created_by', 'updated_by', 'deleted', 'deleted_at', 'deleted_by'], 'default', 'value' => null],
            [['view_count', 'lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator'],
        ]);
    }
	
}
