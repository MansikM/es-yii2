<?php

namespace backend\models\elasticsearch;

use Yii;
use yii\base\Model;
use yii\base\Module;
use yii\elasticsearch\ActiveRecord;

class CardsES extends ActiveRecord
{
    public function attributes()
    {
        return ['id', 'name', 'description', 'image', 'view_count'];
    }

    /**
     * @return string Наименование индекса для этой модели
     */
    public static function index()
    {
        return 'es-yii2-cards';
    }

    /**
     * @return string Наименование типа для этой модели
     */
    public static function type()
    {
        return '_doc';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [$this->attributes(), 'safe']
        ];
    }


    /**
     * @return array Сопоставление для этой модели
     */

    public static function mapping()
    {
        return [
            'properties' => [
                'id'             => ['type' => 'long'],
                'name'           => ['type' => 'text'],
                'description'    => ['type' => 'text'],
                'image'          => ['type' => 'text'],
                'view_count'     => ['type' => 'long'],
            ]
        ];
    }

    /**
     * Установка (update) для этой модели
     */
    public static function updateMapping()
    {
        $db = static::getDb();
        $command = $db->createCommand();
        $command->setMapping(static::index(), static::type(), static::mapping());
    }

    /**
     * Создать индекс этой модели
     */
    public static function createIndex()
    {
        $db = static::getDb();
        $command = $db->createCommand();
        $command->createIndex(static::index(), [
            //'settings' => [ /* ... */ ],
            'mappings' => static::mapping(),
            //'warmers' => [ /* ... */ ],
            //'aliases' => [ /* ... */ ],
            //'creation_date' => '...'
        ]);
    }

    /**
     * Удалить индекс этой модели
     */
    public static function deleteIndex()
    {
        $db = static::getDb();
        $command = $db->createCommand();
        $command->deleteIndex(static::index(), static::type());
    }

    /**
     * Проверить сеществование индекса этой модели
     */
    public static function existIndex()
    {
        $db = static::getDb();
        $command = $db->createCommand();
        return $command->indexExists(static::index());
    }

    /**
     * @param $source_model Model Сохранение данных в ElasticSearch по данным из модели
     * @return mixed
     */
    public static function saveFromModel($source_model, $setPrimaryKey = true){

        //ElasticSearch проверяем наличие индекса, создаем при отсутствии
        if (!self::existIndex()) self::createIndex();

        //Если такой объект есть, то загружаем и обновляем, иначе создаем новый
        $this_model = new self();
        $is_update = false;
        if($setPrimaryKey && !empty($source_model->id)) {
            if (!$this_model = self::get($source_model->id)){
                $this_model = new self();
                $this_model->setPrimaryKey($source_model->id);
            } else {
                $is_update = true;
            }
        }

        $model_attributes = $this_model->attributes();
        $source_atributes = $source_model->attributes();

        $this_model->attributes = [];

        //загружаем данные из основной модели
        foreach ($source_atributes as $source_atribute){
            if (\in_array($source_atribute, $model_attributes, true)){
                $this_model->setAttribute($source_atribute, $source_model->$source_atribute);
            }
        }

        if ($is_update) {
            $this_model->update();
        } else {
            $this_model->save();
        }

        return $this_model->primaryKey;
    }

    /**
     * @param $id
     * @throws \yii\db\StaleObjectException
     * @throws \yii\elasticsearch\Exception
     */
    public static function deleteOne($id){
        $this_model = self::get($id);
        if ($this_model !== null) $this_model->delete();
    }


}
