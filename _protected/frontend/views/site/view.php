<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $model backend\models\Cards */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Карточки', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="cards-view">

    <div class="row">
        <div class="col-sm-11">
            <h2><?= 'Карточка:'.' '. Html::encode($this->title) ?></h2>
        </div>
        <div class="col-sm-1" style="margin-top: 15px">
<?=             
             Html::a('<i class="fa glyphicon glyphicon-hand-up"></i> ' . 'PDF', 
                ['pdf', 'id' => $model->id],
                [
                    'class' => 'btn btn-danger',
                    'target' => '_blank',
                    'data-toggle' => 'tooltip',
                    'title' => 'Will open the generated PDF file in a new window'
                ]
            )?>
        </div>
    </div>

    <div class="row">
<?php

    $this->registerCssFile('https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.1.25/jquery.fancybox.min.css', ['depends' => [\yii\web\JqueryAsset::class]]);
    $this->registerJsFile('https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.1.25/jquery.fancybox.min.js', ['depends' => [\yii\web\JqueryAsset::class]]);
    $this->registerJs('jQuery(document).ready(function(){jQuery("a.fancy-box").fancybox();});', yii\web\View::POS_READY);

    $gridColumn = [
        ['attribute' => 'id', 'visible' => false],
        'name:ntext',
        'description:ntext',
        'view_count',
        [
            'attribute' => 'image',
            'format' => 'html',
            'value' => function ($model) {
                return Html::a(Html::img($model->getUploadUrl(false, false), ['alt' => 'image', 'class' => 'rounded float-left img-thumbnail', 'height' => '50px', 'width' => '50px']), $model->getUploadUrl(false, false), ['alt' => 'image', 'class' => 'fancy-box']);
            },
        ],
        ['attribute' => 'lock', 'visible' => false],
    ];
    echo DetailView::widget([
        'model' => $model,
        'attributes' => $gridColumn
    ]);
?>
    </div>
</div>
