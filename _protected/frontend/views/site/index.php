<?php
/* @var $this yii\web\View */

use yii\data\ArrayDataProvider;
use yii\helpers\Html;
use kartik\export\ExportMenu;
use kartik\grid\GridView;

$this->title = Yii::t('app', Yii::$app->name);
$this->registerCssFile('https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.1.25/jquery.fancybox.min.css', ['depends' => [\yii\web\JqueryAsset::class]]);
$this->registerJsFile('https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.1.25/jquery.fancybox.min.js', ['depends' => [\yii\web\JqueryAsset::class]]);
$this->registerJs('jQuery(document).ready(function(){jQuery("a.fancy-box").fancybox();});', yii\web\View::POS_READY);

?>
<div class="site-index">

    <div class="jumbotron">
        <h2>ElasticSearch Yii2 Test</h2>
    </div>

    <div class="body-content">

        <div class="row">
            <?php
            if ($cards){
                $data = array();
                foreach ($cards as $card){
                    $card_es = new \backend\models\elasticsearch\CardsES($card);
                    $data[] = [
                        'id' => $card_es->getAttribute('id'),
                        'name' => $card_es->getAttribute('name'),
                        'description' => $card_es->getAttribute('description'),
                        'image' => $card_es->getAttribute('image'),
                        'view_count' => $card_es->getAttribute('view_count'),
                    ];
                }

                $dataProvider = new ArrayDataProvider([
                    'allModels' => $data,
                    'pagination' => [
                        'pageSize' => 10,
                    ],
                    'sort' => [
                        'attributes' => ['id', 'name', 'view_count'],
                    ],
                ]);

                $gridColumn = [
                    ['class' => 'yii\grid\SerialColumn'],
                    'id',
                    [
                        'attribute' => 'name',
                        'label' => 'Наименование',
                    ],
                    [
                        'attribute' => 'description',
                        'label' => 'Описание',
                    ],
                    [
                        'attribute' => 'image',
                        'label' => 'Изображение',
                        'format' => 'html',
                        'value' => function ($model) {
                            $card_model = new \backend\models\Cards();
                            $card_model->image = $model['image'];
                            return Html::a(Html::img($card_model->getUploadUrl(false, false), ['alt' => 'image', 'class' => 'rounded float-left img-thumbnail', 'height' => '50px', 'width' => '50px']), $card_model->getUploadUrl(false, false), ['alt' => 'image', 'class' => 'fancy-box']);
                        },
                    ],
                    [
                        'attribute' => 'view_count',
                        'label' => 'Просмотров',
                    ],
                    [
                        'class' => 'yii\grid\ActionColumn',
                        'template' => '{view}',
                        'buttons' => [
                            'view' => function ($url, $model) {
                                $customurl=Yii::$app->getUrlManager()->createUrl(['site/view', 'id'=>$model['id']]);
                                return Html::a('<span class="glyphicon glyphicon-eye-open"></span>', $customurl, ['title' => 'Просмотр']);
                            },
                        ],
                    ],
                ];
                ?>
                <?= GridView::widget([
                    'dataProvider' => $dataProvider,
                    'columns' => $gridColumn,
                    'pjax' => true,
                    'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-cards']],
                    'panel' => [
                        'type' => GridView::TYPE_PRIMARY,
                        'heading' => '<span class="glyphicon glyphicon-book"></span> ' . Html::encode('Последние 6 карточек'),
                    ],
                ]);
            }
            ?>
        </div>

    </div>
</div>

